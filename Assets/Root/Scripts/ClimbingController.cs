﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityStandardAssets.CrossPlatformInput;

public enum WallState
{
    Searching,
    Touching,
    IdleBraced,
    Rotating,
    ClimbingUp,
    ClimbingDown,
    ClimbingLeft,
    ClimbingRight
}
public class ClimbingController : MonoBehaviour
{
    [Header("Climbing Controller")]
    public bool _debug;
    [SerializeField]
    protected WallState _state = WallState.Searching;
    [SerializeField]
    protected WallState _previousState = WallState.Searching;

    [Range(0.1f, 10.0f)]
    public float _jumpForce = 1f;


    public List<MonoBehaviour> _CharacterControllers;


    [Header("Layer Info Data")]
    public LayerMask _wallLayer;
    public LayerMask _handHoldLayer;
    public Transform _handPosition;

    protected Animator _animator;
    protected Rigidbody _rigidbody;
    protected Collider _characterCollider;
    Ray ray;
    RaycastHit hit;

    [Header("Raycast Data")]
    public int _rayCastTotal;
    public float X_SPACE_BETWEEN_CAST;
    public float NUMBER_OF_COLUMN;
    public float Y_SPACE_BETWEEN_CAST;
    public float _cooldownBetweenCast;
    protected float _lastCastTime;
    protected Transform[] _bodyIKs = new Transform[4];

    public Transform _leftFoot, _rightFoot, _leftHand, _rightHand;

    private void Start()
    {
        _state = WallState.Searching;
        _previousState = _state;
        _animator = GetComponent<Animator>();
        _characterCollider = GetComponent<Collider>();
        _rigidbody = GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void Update()
    {
        SearchForWall();
        InputController();
    }

    private void InputController()
    {
        if (!CompareState(WallState.IdleBraced))
            return;
        if (CrossPlatformInputManager.GetAxis("Vertical") > 0.2f)
        {
            BeginClimbUp();
        }
        if (CrossPlatformInputManager.GetAxis("Vertical") < -0.2f)
        {
            BeginClimbDown();
        }
        if (CrossPlatformInputManager.GetAxis("Horizontal") > 0.2f)
        {
            BeginClimbRight();
        }
        if (CrossPlatformInputManager.GetAxis("Horizontal") < -0.2f)
        {
            BeginClimbLeft();
        }
    }

    protected void SearchForWall()
    {
        ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 1.0f, _wallLayer))
        {
            OnBeginTouching();
        }
        if (_debug) { Debug.DrawRay(transform.position, transform.forward, Color.yellow); }
    }
    private void OnBeginTouching()
    {
        if (!CompareState(WallState.Searching))
            return;
        BeginRayCasting();
    }
    protected void BeginClimbUp()
    {
        Transform hit;
        print("trying Climbing up");
        if (SendRayCast(_handPosition.position + new Vector3(0, 0.55f, 0) - transform.forward, transform.forward, out hit, 0))
        {
            print("Climbing up");
            _animator.SetBool("ClimbUp", true);
            AttachToHandle(hit, WallState.ClimbingUp);
        }
    }
    protected void BeginClimbLeft()
    {
        Transform hit;
        print("trying Climbing Left");
        if (SendRayCast(_handPosition.position - new Vector3(0.25f, 0.0f, 0) - transform.forward, transform.forward, out hit, 2))
        {
            print("Climbing Left");
            _animator.SetBool("ClimbUp", true);
            AttachToHandle(hit, WallState.ClimbingLeft, true, false);
        }
    }
    protected void BeginClimbRight()
    {
        Transform hit;
        print("trying Climbing Right");
        if (SendRayCast(_handPosition.position + new Vector3(0.25f, 0.0f, 0) - transform.forward, transform.forward, out hit, 1))
        {
            print("Climbing Right");
            _animator.SetBool("ClimbUp", true);
            AttachToHandle(hit, WallState.ClimbingRight, true, false);
        }
    }
    protected void BeginClimbDown()
    {
        Transform hit;
        if (SendRayCast(_handPosition.position - new Vector3(0, 0.8f, 0) - transform.forward, transform.forward, out hit, 0))
        {
            _animator.SetBool("ClimbDown", true);
            AttachToHandle(hit, WallState.ClimbingDown, false, false);
        }
    }

    private void BeginRayCasting()
    {
        Transform hit;
        if (SendRayCast(_handPosition.position, transform.forward, out hit, 0))
        {
            ChangeState(WallState.IdleBraced);
            AttachToHandle(hit, WallState.ClimbingUp);
            SetUpClimbing();
            if (hit.GetChild(0))
            {
                _leftHand = hit.GetChild(0);
            }
            if (hit.GetChild(1))
            {
                _rightHand = hit.GetChild(1);
            }
            if (hit.GetChild(2))
            {
                _leftFoot = hit.GetChild(2);
            }
            if (hit.GetChild(3))
            {
                _rightFoot = hit.GetChild(3);
            }
        }

    }
    public void SetLeftHand()
    {
        _leftHand = _bodyIKs[0];
    }
    public void SetRightHand()
    {
        _rightHand = _bodyIKs[1];
    }
    public void SetLeftFoot()
    {
        _leftFoot = _bodyIKs[2];
    }
    public void SetRightFoot()
    {
        _rightFoot = _bodyIKs[3];
    }
    protected bool IsOdd(int value)
    {
        return value % 2 != 0;
    }
    protected bool SendRayCast(Vector3 origin, Vector3 direction, out Transform objToReturn, int castFrom)
    {
        if (Time.time - _lastCastTime < _cooldownBetweenCast)
        {
            objToReturn = null;
            return false;
        }
        _lastCastTime = Time.time;
        for (int i = 0; i < _rayCastTotal; i++)
        {
            //Ray ray = new Ray((origin - new Vector3((NUMBER_OF_COLUMN * X_SPACE_BETWEEN_ITEM) / 2, 0, 0)) + new Vector3(X_SPACE_BETWEEN_ITEM * (i % NUMBER_OF_COLUMN), Y_SPACE_BETWEEN_ITEM * (i / NUMBER_OF_COLUMN), 0f), direction);
            Vector3 castSpot = Vector3.zero;
            switch (castFrom)
            {
                case 0:
                    if (IsOdd(i))
                    {
                        castSpot = origin + new Vector3(X_SPACE_BETWEEN_CAST * (i % NUMBER_OF_COLUMN), Y_SPACE_BETWEEN_CAST * (i / NUMBER_OF_COLUMN), 0f);
                    }
                    else
                    {
                        castSpot = origin - new Vector3(X_SPACE_BETWEEN_CAST * (i % NUMBER_OF_COLUMN), -Y_SPACE_BETWEEN_CAST * (i / NUMBER_OF_COLUMN), 0f);
                    }
                    break;
                case 1:
                    castSpot = origin + new Vector3(X_SPACE_BETWEEN_CAST * (i % NUMBER_OF_COLUMN), Y_SPACE_BETWEEN_CAST * (i / NUMBER_OF_COLUMN), 0f);
                    break;
                case 2:
                    castSpot = origin - new Vector3(X_SPACE_BETWEEN_CAST * (i % NUMBER_OF_COLUMN), Y_SPACE_BETWEEN_CAST * (i / NUMBER_OF_COLUMN), 0f);
                    break;
            }
            Ray ray = new Ray(castSpot, direction);
            RaycastHit hit = new RaycastHit();
            Debug.DrawRay(castSpot, direction, Color.black, 5f);
            if (Physics.Raycast(ray, out hit, 1, _handHoldLayer))
            {
                objToReturn = hit.collider.gameObject.transform;
                _bodyIKs[0] = objToReturn.GetChild(0);
                _bodyIKs[1] = objToReturn.GetChild(1);
                _bodyIKs[2] = objToReturn.GetChild(2);
                _bodyIKs[3] = objToReturn.GetChild(3);
                return true;
            }
        }
        objToReturn = null;
        return false;
    }
    protected void SetUpClimbing()
    {
        for (int i = 0; i < _CharacterControllers.Count; i++)
        {
            _CharacterControllers[i].enabled = false;
        }
        _rigidbody.velocity = Vector3.zero;
        _characterCollider.enabled = false;
        _rigidbody.useGravity = false;
        _animator.SetBool("Climbing", true);
    }
    Vector3 lookAtPos = Vector3.zero;
    Transform attachHandPos = null;
    WallState attachWallState = WallState.ClimbingDown;
    private void AttachToHandle(Transform handPosition, WallState state, bool startCorutine = true, bool rotate = true)
    {
        if (rotate)
        {
            lookAtPos = new Vector3(handPosition.position.x, transform.position.y, handPosition.position.z);
            transform.LookAt(lookAtPos);
        }
        if (startCorutine)
            StartCoroutine(MoveToo(handPosition, state));
        else
        {
            attachHandPos = handPosition;
            attachWallState = state;
        }
    }
    public void StartMoveToo()
    {
        StartCoroutine(MoveToo(attachHandPos, attachWallState, 1.0f));
    }
    IEnumerator MoveToo(Transform pos, WallState state, float adjustJumpForce = 0.0f)
    {
        float elapsedTime = 0;
        ChangeState(state);
        while (elapsedTime < (_jumpForce - adjustJumpForce))
        {
            transform.position = Vector3.Lerp(transform.position, pos.position - (transform.GetChild(1).position - transform.position), (elapsedTime / (_jumpForce - adjustJumpForce)));
            transform.rotation = Quaternion.Slerp(transform.rotation, pos.rotation, (elapsedTime / (_jumpForce - adjustJumpForce)));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        if (_animator.GetBool("ClimbUp"))
            _animator.SetBool("ClimbUp", false);
        if (_animator.GetBool("ClimbDown"))
            _animator.SetBool("ClimbDown", false);
        RestoreState();
        // print("Attaching to " + handPosition.name);
        yield return null;
    }
    protected static bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }
    private void RemoveHandHolds()
    {

    }

    public void ChangeState(WallState state)
    {
        if (_state == state)
            return;
        _previousState = _state;
        _state = state;
    }

    protected bool CompareState(WallState state)
    {
        if (_state == state)
            return true;
        else
            return false;
    }
    protected void RestoreState()
    {
        if (_state == _previousState)
            return;
        _state = _previousState;
    }
    private void OnAnimatorIK(int layerIndex)
    {
        if (_leftFoot)
        {
            SetUpIk(AvatarIKGoal.LeftFoot, _leftFoot.position, _animator.GetFloat("LeftFootWeight"), _leftFoot.rotation);
        }
        if (_rightFoot)
        {
            SetUpIk(AvatarIKGoal.RightFoot, _rightFoot.position, _animator.GetFloat("RightFootWeight"), _rightFoot.rotation);
        }
        if (_leftHand)
        {
            SetUpIk(AvatarIKGoal.LeftHand, _leftHand.position, _animator.GetFloat("LeftHandWeight"), _leftHand.rotation);
        }
        if (_rightHand)
        {
            SetUpIk(AvatarIKGoal.RightHand, _rightHand.position, _animator.GetFloat("RightHandWeight"), _rightHand.rotation);
        }
    }
    protected void SetUpIk(AvatarIKGoal goal, Vector3 pos, float weight, Quaternion rotation)
    {
        _animator.SetIKPosition(goal, pos);
        _animator.SetIKPositionWeight(goal, weight);
        _animator.SetIKRotation(goal, rotation);
    }
}

[Serializable]
public class RayCastInfo
{
    public Vector3 point;
    public Vector3 normal;
}